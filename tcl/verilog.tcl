# Get all setup varibales from bash enviroment
set xdc_dir $env(XDCDIR)
set verilog_dir $env(VERILOGDIR)
set netlist_dir $env(NETLISTDIR)
set coord_dir $env(COORDDIR)
set design_name $env(bmark)
set tmodule $env(top_module)

# Setting design, constraint file/timing target, and netlist to be dumped
set design_file_name [glob ${verilog_dir}/*.v]
set constr [file join $xdc_dir $design_name.xdc]

set netlist_synth [file join $netlist_dir ${design_name}_verilog_netlist_synth.v]
set edif_synth [file join $netlist_dir ${design_name}_verilog_netlist_synth.edn]

set netlist_opt [file join $netlist_dir ${design_name}_verilog_netlist_opt.v]
set edif_opt [file join $netlist_dir ${design_name}_verilog_netlist_opt.edn]

set netlist_place [file join $netlist_dir ${design_name}_verilog_netlist_place.v]
set edif_place [file join $netlist_dir ${design_name}_verilog_netlist_place.edn]

set netlist_route [file join $netlist_dir ${design_name}_verilog_netlist_route.v]
set edif_route [file join $netlist_dir ${design_name}_verilog_netlist_route.edn]

set coordinate_file_place_name [file join $coord_dir ${design_name}_verilog_netlist_place.loc]
set coordinate_file_route_name [file join $coord_dir ${design_name}_verilog_netlist_route.loc]

# Setting device part
set device_part xcvu065-ffvc1517-3-e

# Starting design operation synthesis, placement, routing
create_project $design_name ./ -part $device_part -force
add_files -norecurse -scan_for_includes $design_file_name
import_files -norecurse $design_file_name
add_files -fileset constrs_1 -norecurse $constr
import_files -fileset constrs_1 $constr

synth_design -flatten_hierarchy full -top $tmodule -resource_sharing on
config_timing_corners -corner Slow -delay_type max
write_checkpoint -force post_synth.dcp
report_timing_summary -file post_synth_timing.rpt
report_utilization -file post_synth_util.rpt
# Writing design netlist for further processing in Python later for graph learning
write_verilog -cell $tmodule -mode design -force $netlist_synth
write_edif -force $edif_synth

opt_design
write_checkpoint -force post_opt.dcp
report_timing_summary -file post_opt_timing.rpt
report_utilization -file post_opt_util.rpt
# Writing design netlist for further processing in Python later for graph learning
write_verilog -cell $tmodule -mode design -force $netlist_opt
write_edif -force $edif_opt

place_design
write_checkpoint -force post_place.dcp
report_timing_summary -file post_place_timing.rpt
report_utilization -file post_place_util.rpt
# Get cells for the current design
set coordinate_file_place [open $coordinate_file_place_name w]
set all_leaf_cells [get_cells -hier -filter {IS_PRIMITIVE && STATUS==PLACED}]
foreach my_cell $all_leaf_cells {
    set cell_loc_x [get_property RPM_X [get_sites -of [get_cells $my_cell]]]
    set cell_loc_y [get_property RPM_Y [get_sites -of [get_cells $my_cell]]]
    puts $coordinate_file_place "$my_cell $cell_loc_x $cell_loc_y"
}
close $coordinate_file_place
# Writing design netlist for further processing in Python later for graph learning
write_verilog -cell $tmodule -mode design -force $netlist_place
write_edif -force $edif_place

route_design
write_checkpoint -force post_route.dcp
report_timing_summary -file post_route_timing.rpt
report_utilization -file post_route_util.rpt

# Get cells for the current design
set coordinate_file_route [open $coordinate_file_route_name w]
set all_leaf_cells [get_cells -hier -filter {IS_PRIMITIVE && STATUS==PLACED}]
foreach my_cell $all_leaf_cells {
    set cell_loc_x [get_property RPM_X [get_sites -of [get_cells $my_cell]]]
    set cell_loc_y [get_property RPM_Y [get_sites -of [get_cells $my_cell]]]
    puts $coordinate_file_route "$my_cell $cell_loc_x $cell_loc_y"
}
close $coordinate_file_route
# Writing design netlist for further processing in Python later for graph learning
write_verilog -cell $tmodule -mode design -force $netlist_route
write_edif -force $edif_route
