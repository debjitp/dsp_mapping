#!/bin/bash

# Setting up the working directories

no_ops='7'
op_sizes='32'
WORKDIR=$PWD
## Running all benchmarks
for no_op in $no_ops
do
    export NO_OP=${no_op}
    benchmark=${no_op}_add
    echo -e "\n\n"
    echo -e "Working on: "$benchmark
    export bmark=$benchmark
    for op_size in $op_sizes
    do
        export OP_SIZE=${op_size}
        echo -e "Working on no of operands: "$op_size
       
        # Doing HLS
        cd ${WORKDIR}/Vivado_HLS/$benchmark
        #vivado_hls -f ../tcl/add.tcl -l vivado_hls_${op_size}.log
        cd -
        # Doing normal Vivado flow        
        COORD=$WORKDIR/coord
        RUNDIR=$WORKDIR/Vivado
        VERILOGSRCDIR=$WORKDIR/verilog
        NETLIST=$WORKDIR/netlist
        top_module=my_${no_op}add

        export TCLDIR=$WORKDIR/tcl
        export XDCDIR=$WORKDIR/xdc
        export top_module
        export NETLISTDIR=$NETLIST/$benchmark/$op_size
        export VERILOGDIR=$VERILOGSRCDIR/$benchmark/$op_size
        export COORDDIR=$COORD/$benchmark/$op_size

        if [ ! -d $RUNDIR/$benchmark/$op_size ]
        then
            echo -e "Making directory: "$RUNDIR/$benchmark/$op_size
            mkdir -pv $RUNDIR/$benchmark/$op_size
        fi
    
        if [ ! -d $NETLISTDIR ]
        then
            echo -e "Making directory: "$NETLISTDIR
            mkdir -pv $NETLISTDIR
        fi
    
        if [ ! -d $COORDDIR ]
        then
            echo -e "Making directory: "$COORDDIR
            mkdir -pv $COORDDIR
        fi

        if [ ! -d $VERILOGDIR ]
        then
            echo -e "Making directory: "$VERILOGDIR
            mkdir -pv $VERILOGDIR
        fi

        echo -e "Soft linking synthesized verilog for netlist export"
        ln -s ${WORKDIR}/Vivado_HLS/${benchmark}/proj_${op_size}/sol1/impl/verilog/my_${no_op}add.v $VERILOGDIR/my_${no_op}add.v
    
        cd $RUNDIR/$benchmark/$op_size
        vivado -mode batch -source $TCLDIR/verilog.tcl
        cd -

        unset OP_SIZE
        unset NETLISTDIR
        unset VERILOGDIR
        unset COORDIR
        unset TCLDIR
        unset XDCDIR
        unset top_module
    done
    unset NO_OP
    unset bmark
done
