#include "add.h"

#ifdef FSIZE_32
fsize_32 my_2add(fsize_32 x1, fsize_32 x2){
    fsize_32 y;
#elif FSIZE_25
fsize_25 my_2add(fsize_25 x1, fsize_25 x2){
    fsize_25 y;
#elif FSIZE_16
fsize_16 my_2add(fsize_16 x1, fsize_16 x2){
    fsize_16 y;
#elif FSIZE_12
fsize_12 my_2add(fsize_12 x1, fsize_12 x2){
    fsize_12 y;
#elif FSIZE_8
fsize_8 my_2add(fsize_8 x1, fsize_8 x2){
    fsize_8 y;
#endif
    y = x1 + x2;
    return y;
}
