#include "add.h"

#ifdef FSIZE_32
fsize_32 my_4add(fsize_32 a, fsize_32 b, fsize_32 c, fsize_32 d){
    fsize_32 e;
#elif FSIZE_25
fsize_25 my_4add(fsize_25 a, fsize_25 b, fsize_25 c, fsize_25 d){
    fsize_25 e;
#elif FSIZE_16
fsize_16 my_4add(fsize_16 a, fsize_16 b, fsize_16 c, fsize_16 d){
    fsize_16 e;
#elif FSIZE_12
fsize_12 my_4add(fsize_12 a, fsize_12 b, fsize_12 c, fsize_12 d){
    fsize_12 e;
#elif FSIZE_8
fsize_8 my_4add(fsize_8 a, fsize_8 b, fsize_8 c, fsize_8 d){
    fsize_8 e;
#endif
    e = a + b + c + d;
    return e;
}
