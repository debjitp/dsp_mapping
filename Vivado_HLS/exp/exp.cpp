#include "exp.h"
/*
template<typename NUMBER_TYPE>
fsize_32 my_cosine(NUMBER_TYPE& x) {
    return hls::cos(x);
}

int main(int argc, char **argv) {
    fsize_32 x, y;
    y = my_cosine<fsize_32>(x);
    return 0;
}
*/

fsize_32 my_exp(fsize_32 x){
    return hls::exp(x);
}
