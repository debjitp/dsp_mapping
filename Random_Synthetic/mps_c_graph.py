import argparse
import networkx as nx
import numpy as np
import random, fnmatch
import os
import multiprocessing as mps
from joblib import Parallel, delayed
import subprocess as sbp
from datetime import datetime as dt
import shutil
import logging
from threading import Thread, Event
from queue import Queue
from subprocess import PIPE

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKBLACK  = "\033[90m"
    OKBCKGRD = "\033[47m"
    OKGREEN = '\033[92m'
    OKTEAL = '\033[96m'
    TRY = '\033[97m'
    WARNING = '\033[93m'
    BCKGRD_WARNING = "\033[93m"
    FAIL = '\033[91m'
    BCKGRD_FAIL = "\033[93m"
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def print_warning(print_string):
    logger = logging.getLogger('random')
    logger.warning(print_string)
    print(bcolors.BOLD  + bcolors.OKBLACK + bcolors.BCKGRD_WARNING + "[WARN]--> " + \
            print_string + bcolors.ENDC + " ")

def print_info(print_string):
    logger = logging.getLogger('random')
    logger.info(print_string)
    print(bcolors.BOLD + bcolors.OKTEAL + bcolors.OKGREEN + "[INFO]--> " + print_string + bcolors.ENDC + " ")

def print_fail(print_string):
    logger = logging.getLogger('random')
    logger.critical(print_string)
    print(bcolors.BOLD  + bcolors.OKBLACK + bcolors.FAIL +  "[FAIL]--> " + print_string + bcolors.ENDC + " ")

def random_logger(name, logname):
    formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - \
            - %(message)s')
    handler = logging.FileHandler(logname, mode='w')
    handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)

    return logger


def getTerminalSize():
    import os
    env = os.environ
    def ioctl_GWINSZ(fd):
        try:
            import fcntl, termios, struct, os
            cr = struct.unpack('hh', fcntl.ioctl(fd, termios.TIOCGWINSZ, '1234'))
        except:
            return
        return cr
    cr = ioctl_GWINSZ(0) or ioctl_GWINSZ(1) or ioctl_GWINSZ(2)
    if not cr:
        try:
            fd = os.open(os.ctermid(), os.O_RDONLY)
            cr = ioctl_GWINSZ(fd)
            os.close(fd)
        except:
            pass

    if not cr:
        cr = (env.get('LINES', 25), env.get('COLUMNS', 80))
    return int(cr[1]), int(cr[0])


def rand_design(num_ops, num_mul, idx, out_dir, n_inputs):
    ### generate random graph
    #num_ops = 10
    num_inputs = num_ops + 1
    #num_mul = 2
    add_sub_ops = num_ops - num_mul
    add_ops = np.random.randint(add_sub_ops, size=1)[0]
    sub_ops = add_sub_ops - add_ops
    op_list = ["+"] * add_ops + ["-"] * sub_ops + ["*"] * num_mul
    random.shuffle(op_list)

    G = nx.DiGraph(directed=True)
    types = {}
    for i in range(num_inputs):
        G.add_node(i, attribute='input')
        types[i] = 'In'
    g = G.copy()
    for i in range(num_ops):
        candi = random.sample(g.nodes(), 2)
        g.remove_node(candi[0])
        g.remove_node(candi[1])
        G.add_node(num_inputs+i, attribute=op_list[i])
        g.add_node(num_inputs+i, attribute=op_list[i])
        G.add_edge(candi[0], num_inputs+i)
        G.add_edge(candi[1], num_inputs+i)
        types[i+num_inputs] = op_list[i]

    ### convert graph to C program
    indent = "    "
    bitwidth = ["int8", "int12", "int15", "int16", "int18", "int22", "int25", "int27", "int30", "int32"]
    #bitwidth = ["int18"]

    ### reduce number of inputs
    num_new_inputs = n_inputs
    original_inputs = list(range(num_inputs))
    new_inputs = list(range(num_new_inputs))
    random.shuffle(original_inputs)
    random.shuffle(new_inputs)
    assert len(original_inputs) >= len(new_inputs),"#new_inputs is grater than #original_inputs."
    input_map = {}
    while len(original_inputs) != len(new_inputs):
        input_map[original_inputs[-1]] = random.choice(new_inputs)
        del original_inputs[-1]
    for i in range(len(original_inputs)):
        input_map[original_inputs[i]] = new_inputs[i]

    with open("{}/funct_{}_{}_{}_{}.c".format(out_dir, num_ops, num_mul, n_inputs, idx), "w") as ff:
        funct_output_bitwidth = random.choice(bitwidth)
        ff.write('/* Automatically generated C Code. DO NOT MODIFY.*/\n')
        ff.write('/* Generated at: ' + dt.now().strftime('%b-%d-%Y %I:%M:%S %p') + '*/\n\n')
        ff.write("#include \"ap_cint.h\"\n")
        ff.write("\n")
        ff.write(funct_output_bitwidth + " funct_{}_{}_{}_{}(".format(num_ops, num_mul, n_inputs, idx))
        input_bitwidth = []
        string =[]
        for i in range(num_new_inputs):
            input_bitwidth.append(random.choice(bitwidth))
            string.append(input_bitwidth[i] + ' a' + str(i))
        ff.write(', '.join(string))
        del string
        ff.write(") {\n")
        ff.write("#pragma HLS LATENCY max=0 min=0\n")
        for j in range(num_ops-1):
            ff.write(indent + random.choice(bitwidth) + " a{};\n".format(j+num_inputs))
        ff.write('\n' * 2)
        ff.write(indent + funct_output_bitwidth + " funct_output;\n")
        ff.write('\n' * 2)
        for j in range(num_ops-1):
            ins = list(G.predecessors(j+num_inputs))
            if ins[0] in input_map:
                ins[0] = input_map[ins[0]]
            if ins[1] in input_map:
                ins[1] = input_map[ins[1]]
            ff.write(indent + "a{} = a{} {} a{};\n".format(j+num_inputs, ins[0],\
                    G.nodes[j+num_inputs]["attribute"], ins[1]))
        ins = list(G.predecessors(num_ops-1+num_inputs))
        if ins[0] in input_map:
            ins[0] = input_map[ins[0]]
        if ins[1] in input_map:
            ins[1] = input_map[ins[1]]
        ff.write(indent + "funct_output = a{} {} a{};\n\n".format(ins[0],\
                G.nodes[num_ops-1+num_inputs]["attribute"], ins[1]))
        ff.write(indent + "return funct_output;\n")
        ff.write("}")

        ff.write('\n' * 2)
        ff.write('int main(int argc, char **argv) {\n')
        for i in range(num_new_inputs):
            ff.write(indent + input_bitwidth[i] + " a{};\n".format(i))
        ff.write('\n' * 2)
        ff.write(indent + funct_output_bitwidth + " funct_output;\n")
        ff.write('\n' * 2)
        ff.write(indent + 'funct_output = funct_{}_{}_{}_{}('.format(num_ops, num_mul, n_inputs, idx) + \
                ', '.join(['a' + str(i) for i in range(num_new_inputs)]) + ');\n\n')
        ff.write(indent + 'return 0;\n}')
        del input_bitwidth

def get_file_names_from_loc(root_dir, extension):
    name_of_files = []
    for root, dirNames, fileNames in os.walk(root_dir):
        for fileName in fnmatch.filter(fileNames, extension):
            name_of_files.append(os.path.abspath(os.path.join(root, fileName)))

    return name_of_files

def exec_command(command, out_file_name, err_file_name):

    done = Event()
    
    if out_file_name == 'pipe' or out_file_name == 'PIPE':
        out = PIPE
    else:
        out = open(out_file_name, 'w')

    if err_file_name == 'pipe' or err_file_name == 'PIPE':
        err = PIPE
    else:
        err = open(err_file_name, 'w')

    proc = sbp.Popen(command, shell=True, stdout=out, stderr=err)

    data, stderr = proc.communicate()
    done.set()

    if not out_file_name == 'pipe' and not out_file_name == 'PIPE':
        out.close()

    if not err_file_name == 'pipe' and not err_file_name == 'PIPE':
        err.close()

    return data, stderr, proc.returncode

def worker(inputQ, outputQ):
    for func, args in iter(inputQ.get, 'STOP'):
        result = calculate(func, args)
        outputQ.put(result)

def calculate(func, args):
    result = func(*args)
    return result

def parallel_hls_synthesis(name_of_designs, hls_path, NUMBER_OF_PROCESSES):

    i = 0

    # Prepping for splitting all designs across the number of specified cores
    designs_per_core = []
    for idx in range(NUMBER_OF_PROCESSES):
        designs_per_core.append([])

    for design in name_of_designs:
        designs_per_core[i % NUMBER_OF_PROCESSES].append(design)
        i = i + 1
        if i == NUMBER_OF_PROCESSES:
            i = 0

    task_queue = mps.Queue()
    done_queue = mps.Queue()
    
    TASKS = [(run_hls, (designs_per_core[i], \
                        hls_path, \
                        i)) \
                        for i in range(NUMBER_OF_PROCESSES) if designs_per_core[i]]

    for task in TASKS:
        task_queue.put(task)

    for i in range(len(TASKS)):
        mps.Process(target=worker, args=(task_queue, done_queue)).start()

    for i in range(len(TASKS)):
        done_queue.get()

    for i in range(len(TASKS)):
        task_queue.put('STOP')

    return

def run_hls(designs, hls_path, cpu_num):
    '''
    pbar = tqdm(total=len(designs), \
                desc='Doing HLS Synthesis and Netlist extraction via (CPU: {})'.format(cpu_num)
                )
    '''
    for design in designs:
        #Status = False
        #pbar.update(1)
        print_info('Doing HLS for design %s via CPU: %d.\n' % (design, cpu_num))
        design_name = design[design.rfind('/') + 1 : design.rfind('.')]
        synth_path = os.path.join(hls_path, design_name)
        if not os.path.isdir(synth_path):
            os.mkdir(synth_path)
        hls_tcl_file = synth_path + '/' + design_name + '_hls.tcl'
        tcl = open(hls_tcl_file, 'w')
        tcl.write('# Automatically generated TCL file. DO NOT MODIFY.\n')
        tcl.write('# Generated at: ' + dt.now().strftime('%b-%d-%Y %I:%M:%S %p') + '\n\n')
        tcl.write('open_project -reset proj\n')
        tcl.write('add_files %s\n' % (design))
        tcl.write('set_top %s\n' % (design_name))
        tcl.write('open_solution -reset sol1\n')
        tcl.write('set_part {xcvu11p-flgb2104-2-e}\n')
        tcl.write('create_clock -period 3.33\n')
        tcl.write('csynth_design\n')
        tcl.write('export_design -flow impl -rtl verilog\n')
        tcl.write('close_project\n')
        tcl.write('quit')
        tcl.close()
        
        #pwd = os.getcwd()
        os.chdir(os.path.join(hls_path, design_name))

        vivado_hls_cmd = 'nice -n 10 vivado_hls -f ' + hls_tcl_file
        data, err, retcode = exec_command(vivado_hls_cmd, \
                                          os.path.join(os.getcwd(), 'vivado_hls.log'), \
                                          os.path.join(os.getcwd(), 'vivado_hls.err')
                                          )
        dcp_name = os.path.join(hls_path, \
                                design_name, \
                                'proj/sol1/impl/verilog/project.runs/impl_1/bd_0_wrapper.dcp')
        if not os.path.exists(dcp_name):
            print_info('HLS for design: %s failed.' % (design_name))
            os.system('touch HLS_FAILURE')
        else:
            #Status = True
            os.system('touch HLS_SUCCESS')

        os.chdir(hls_path)
        '''
        if Status:
            run_netlist_extraction([design], hls_path, netlist_path, cpu_num)
        '''

    #pbar.close()
    return True
    
def parallel_netlist_extraction(name_of_designs, hls_path, netlist_path, NUMBER_OF_PROCESSES):

    i = 0

    # Prepping for splitting all designs across the number of specified cores
    designs_per_core = []
    for idx in range(NUMBER_OF_PROCESSES):
        designs_per_core.append([])

    for design in name_of_designs:
        designs_per_core[i % NUMBER_OF_PROCESSES].append(design)
        i = i + 1
        if i == NUMBER_OF_PROCESSES:
            i = 0

    task_queue = mps.Queue()
    done_queue = mps.Queue()
    
    TASKS = [(run_netlist_extraction, (designs_per_core[i], \
                        hls_path, \
                        netlist_path, \
                        i)) \
                        for i in range(NUMBER_OF_PROCESSES) if designs_per_core[i]]

    for task in TASKS:
        task_queue.put(task)

    for i in range(len(TASKS)):
        mps.Process(target=worker, args=(task_queue, done_queue)).start()

    for i in range(len(TASKS)):
        done_queue.get()

    for i in range(len(TASKS)):
        task_queue.put('STOP')

    return

def run_netlist_extraction(designs, hls_path, netlist_path, cpu_num):

    for design in designs:
        print('Extracting netlist for the design %s via CPU: %d.\n' % (design, cpu_num))
        design_name = design[design.rfind('/') + 1 : design.rfind('.')]
        synth_path = os.path.join(hls_path, design_name)
        if not os.path.isdir(synth_path):
            os.mkdir(synth_path)

        extraction_path = os.path.join(netlist_path, design_name)
        if not os.path.isdir(extraction_path):
            os.mkdir(extraction_path)

        nextraction_tcl_file = synth_path + '/' + design_name + '_nextraction.tcl'
        tcl = open(nextraction_tcl_file, 'w')
        tcl.write('# Automatically generated TCL file. DO NOT MODIFY.\n')
        tcl.write('# Generated at: ' + dt.now().strftime('%b-%d-%Y %I:%M:%S %p') + '\n\n')
        tcl.write('write_verilog -cell bd_0_hls_inst_0_' + design_name + ' -mode design -force ' + \
                os.path.join(extraction_path, design_name + '_verilog_netlist_synth.v') + \
                ' -rename_top ' + design_name + '\n')
        tcl.write('close_project')
        tcl.close()
        
        os.chdir(os.path.join(hls_path, design_name))

        vivado_nextraction_cmd = 'nice -n 10 vivado ' + \
                'proj/sol1/impl/verilog/project.runs/impl_1/bd_0_wrapper.dcp ' + \
                '-mode batch -appjournal -source ' + nextraction_tcl_file + ' ' + \
                '-l ' + os.path.join(os.getcwd(), 'vivado_nextraction.log') 
        data, err, retcode = exec_command(vivado_nextraction_cmd, \
                                          'PIPE', \
                                          os.path.join(os.getcwd(), 'vivado_nextraction.err')
                                          )

        if not os.path.exists(os.path.join(extraction_path, \
                                           design_name + '_verilog_netlist_synth.v')):
            print_warning('Netlist extraction for design: %s failed.' % (design_name))
            os.system('touch NETLIST_FAILURE')
        else:
            os.system('touch NETLIST_SUCCESS')

        os.chdir(hls_path)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-n','--num_ops', type=int,\
                        default=10,\
                        help='number of ops\
                        Integer. Default:10')
    parser.add_argument('-p','--inputs', type=int,\
                        default=6,\
                        help='number of inputs\
                        Integer. Default:6')
    parser.add_argument('-m','--num_mul', type=int,\
                        default=2,\
                        help='number of multiply\
                        Integer. Default:2')
    parser.add_argument('-i','--index', type=int,\
                        default=1,\
                        help='design index\
                        Integer. Default:1')
    parser.add_argument('-o', '--out_dir', type=str,\
                        default='./',\
                        help='file name to output directory\
                        String. Default:funct'
                        )
    parser.add_argument('-s', '--hls_dir', type=str,\
                        default='./',\
                        help='hls directory\
                        String. Default:hls'
                        )
    parser.add_argument('-e', '--netlist_dir', type=str,\
                        default='./',\
                        help='netlist directory\
                        String. Default:hls'
                        )
    parser.add_argument('-N', '--no_of_proc', type=int,\
                        default=2,\
                        help='Number of processor core for parallel compilation\
                        Integer. Default:2'
                        )

    '''
    How to run:
    python c_graph.py -n 10 -p 5 -m 3 -i 3 -N 2 -o c_code.3 -s Vivado_HLS.3 -e netlist.3
    '''
    FLAGS, unknown=parser.parse_known_args()
   
    # Maximum number of operations
    num_ops = FLAGS.num_ops
    # Maximum number of multiplications
    num_mul = FLAGS.num_mul
    # Generated design index
    idx     = FLAGS.index
    # Output directory name 
    out_dir = FLAGS.out_dir
    # Maximum number of inputs
    n_inputs = FLAGS.inputs
    # HLS synthesis directory
    hls_dir = FLAGS.hls_dir
    # Netlist directory
    netlist_dir = FLAGS.netlist_dir
    # No of processors for parallel compilation
    NUMBER_OF_PROCESSES = FLAGS.no_of_proc

    # Original function call
    #rand_design(num_ops, num_mul, idx, out_dir, n_inputs)


    # Modified function call
    src_path = os.path.join(os.getcwd(), out_dir)
    if not os.path.isdir(src_path):
        os.mkdir(src_path)

    hls_path = os.path.join(os.getcwd(), hls_dir)
    if not os.path.isdir(hls_path):
        os.mkdir(hls_path)
    
    netlist_path = os.path.join(os.getcwd(), netlist_dir)
    if not os.path.isdir(netlist_path):
        os.mkdir(netlist_path)

    logname = os.path.join(hls_path, 'random.log')
    rlogger = random_logger('random', logname)

    # Generate C code
    for n in range(num_ops, num_ops + 1):
        for m in range(2, num_mul + 1):
            for p in range(3, n_inputs + 1):
                for i in range(1, idx + 1):
                    print_info('Generating design Idx: %d Inputs: %d Total Ops: %d Mul Ops: %d Add Ops: %d' % (i, p, n, m, n - m))
                    rand_design(n, m, i, src_path, p)
    
    print('\n' * 3)
    # HLS Synthesis
    name_of_designs = get_file_names_from_loc(src_path, '*.c')
    
    print_info('Synthesizing generated C designs using Vivado HLS. Be patinet. It will take a while.\n\n')
    name_of_designs_incomplete = name_of_designs[:]
    iteration = 0
    while name_of_designs_incomplete:
        parallel_hls_synthesis(name_of_designs_incomplete, hls_path, NUMBER_OF_PROCESSES)
        Q = []
        for design in name_of_designs_incomplete:
            design_name = design[design.rfind('/') + 1 : design.rfind('.')]
            dcp_name = os.path.join(hls_path, \
                                    design_name, \
                                    'proj/sol1/impl/verilog/project.runs/impl_1/bd_0_wrapper.dcp')
            if not os.path.exists(dcp_name):
                Q.append(design)
                try:
                    shutil.rmtree(os.path.join(hls_path, \
                                               design_name
                                               ))
                except OSError as e:
                    print_fail('Error: %s - %s.' % (e.filename, e.strerror))
        print('\n' * 2)
        if len(Q) == len(name_of_designs_incomplete):
            iteration = iteration + 1
        else:
            iteration = 0

        if iteration >= 3:
            print_fail('Tried maximum time recompilation.\nFollowing designs need manual intervention: %s\n' % ('\n'.join(Q)))
            break
        if Q:
            print_warning('Trying to recompile follwing designs: %s\n' % ('\n'.join(Q)))
        print('\n' * 2)
        name_of_designs_incomplete = Q[:]
        del Q
    
    print_info('Extracting netlists using Vivado... Be patinet\n\n')
    name_of_designs_incomplete = name_of_designs[:]
    iteration = 0
    while name_of_designs_incomplete:
        parallel_netlist_extraction(name_of_designs, hls_path, netlist_path, NUMBER_OF_PROCESSES)
        Q = []
        for design in name_of_designs_incomplete:
            design_name = design[design.rfind('/') + 1 : design.rfind('.')]
            dcp_name = os.path.join(hls_path, \
                                    design_name, \
                                    'proj/sol1/impl/verilog/project.runs/impl_1/bd_0_wrapper.dcp')
            netlist_name = os.path.join(netlist_path, \
                                        design_name, \
                                        design_name + '_verilog_netlist_synth.v')
            if not os.path.exists(dcp_name):
                print_warning('Netlist for design %s cannot be extracted as HLS for it failed' % (design))
            elif os.path.exists(dcp_name) and not os.path.exists(netlist_name):
                Q.append(design)
                try:
                    os.remove(os.path.join(hls_path, \
                                           design_name, \
                                           'vivado_nextraction.log'
                                           ))
                    os.remove(os.path.join(hls_path, \
                                           design_name, \
                                           'NETLIST_FAILURE'
                                          ))

                except OSError as e:
                    print_fail('Error: %s - %s.' % (e.filename, e.strerror))
        print('\n' * 2)
        if len(Q) == len(name_of_designs_incomplete):
            iteration = iteration + 1
        else:
            iteration = 0

        if iteration >= 3:
            print_fail('Tried maximum time netlist extraction.\nFollowing designs need manual intervention: %s\n' % ('\n'.join(Q)))
            break
        if Q:
            print_warning('Trying to recompile following designs: %s\n' % ('\n'.join(Q)))
        print('\n' * 2)
        name_of_designs_incomplete = Q[:]
        del Q
